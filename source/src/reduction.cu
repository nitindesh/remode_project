// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <rmd/reduction.cuh>
#include "reduction_kernels.cu"

rmd_ImageReducer::rmd_ImageReducer(dim3 num_threads_per_block,
                                   dim3 num_blocks_per_grid)
  : block_dim_(num_threads_per_block)
  , grid_dim_(num_blocks_per_grid)
{
  // Compute required amount of shared memory
  sh_mem_size_ = block_dim_.x * block_dim_.y * sizeof(int);

  // Allocate intermediate result
  const cudaError part_alloc_err = cudaMallocPitch(
        &dev_partial_,
        &dev_partial_pitch_,
        grid_dim_.x*sizeof(int),
        grid_dim_.y);
  CHECK_ERROR(part_alloc_err, "ImageReducer: unable to allocate pitched device memory for partial results");
  dev_partial_stride_ = dev_partial_pitch_ / sizeof(int);

  // Allocate final result
  const cudaError fin_alloc_err = cudaMalloc(&dev_final_, sizeof(int));
  CHECK_ERROR(fin_alloc_err, "ImageReducer: unable to allocate device memory for final result");
}

rmd_ImageReducer::~rmd_ImageReducer()
{
  cudaError err = cudaFree(dev_final_);
  CHECK_ERROR(err, "ImageReducer: unable to free device memory");
  err = cudaFree(dev_partial_);
  CHECK_ERROR(err, "ImageReducer: unable to free device memory");
}

// Count elements equal to 'value'
// First count over the thread grid,
// then perform a reduction sum on a single thread block
size_t rmd_ImageReducer::countEqual(const int* in_img_data,
                                    size_t in_img_stride,
                                    size_t in_img_width,
                                    size_t in_img_height,
                                    int value)
{
  rmd_reductionCountEqKernel<<<grid_dim_, block_dim_, sh_mem_size_>>>(
    dev_partial_,
    dev_partial_stride_,
    in_img_data,
    in_img_stride,
    in_img_width,
    in_img_height,
    value);

  rmd_reductionSumKernel
      <<<1, block_dim_, sh_mem_size_>>>
                                      (dev_final_,
                                       0,
                                       dev_partial_,
                                       dev_partial_stride_,
                                       grid_dim_.x,
                                       grid_dim_.y);

  // download sum
  int h_count;
  const cudaError err = cudaMemcpy(&h_count, dev_final_, sizeof(int), cudaMemcpyDeviceToHost);
  CHECK_ERROR(err, "countEqual: unable to copy result from device to host");

  return static_cast<size_t>(h_count);
}

size_t rmd_ImageReducer::countEqual(const rmd_DeviceImage& in_img, int value)
{
  return rmd_ImageReducer::countEqual((int*)in_img.data,
                                      in_img.stride,
                                      in_img.width,
                                      in_img.height,
                                      value);
}
