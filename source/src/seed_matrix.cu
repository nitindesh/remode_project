// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <rmd/seed_matrix.cuh>

#include <rmd/convergence_states.cuh>
#include <rmd/helper_vector_types.cuh>

#include "epipolar_match.cu"
#include "seed_check.cu"
#include "seed_init.cu"
#include "seed_update.cu"

rmd::SeedMatrix::SeedMatrix(
    CLEnv& env,
    const size_t &width,
    const size_t &height,
    const rmd_PinholeCamera &cam)
  : width_(width)
  , height_(height)
{
  rmd_DeviceImage_init(&ref_img_, sizeof(float), width, height);
  rmd_DeviceImage_init(&curr_img_, sizeof(float), width, height);
  rmd_DeviceImage_init(&sum_templ_, sizeof(float), width, height);
  rmd_DeviceImage_init(&const_templ_denom_, sizeof(float), width, height);
  rmd_DeviceImage_init(&mu_, sizeof(float), width, height);
  rmd_DeviceImage_init(&sigma_, sizeof(float), width, height);
  rmd_DeviceImage_init(&a_, sizeof(float), width, height);
  rmd_DeviceImage_init(&b_, sizeof(float), width, height);
  rmd_DeviceImage_init(&convergence_, sizeof(int), width, height);
  rmd_DeviceImage_init(&epipolar_matches_, sizeof(float2), width, height);

  rmd_mvs_DeviceData_init(&dev_data_);

  // Save image details to be uploaded to device memory
  dev_data_.ref_img = ref_img_.dev_ptr;
  dev_data_.curr_img = curr_img_.dev_ptr;
  dev_data_.sum_templ = sum_templ_.dev_ptr;
  dev_data_.const_templ_denom = const_templ_denom_.dev_ptr;
  dev_data_.mu = mu_.dev_ptr;
  dev_data_.sigma = sigma_.dev_ptr;
  dev_data_.a = a_.dev_ptr;
  dev_data_.b = b_.dev_ptr;
  dev_data_.convergence = convergence_.dev_ptr;
  dev_data_.epipolar_matches = epipolar_matches_.dev_ptr;

  // Save camera parameters
  dev_data_.cam = cam;
  dev_data_.one_pix_angle = rmd_PinholeCamera_getOnePixAngle(&cam);
  dev_data_.width  = width;
  dev_data_.height = height;
  // Device image size
  host_img_size_.width  = width_;
  host_img_size_.height = height_;

  // Kernel configuration for depth estimation
  dim_block_.x = 16;
  dim_block_.y = 16;
  dim_grid_.x = (width  + dim_block_.x - 1) / dim_block_.x;
  dim_grid_.y = (height + dim_block_.y - 1) / dim_block_.y;

  // Image reducer to compute statistics on seeds
  dim3 num_threads_per_block;
  dim3 num_blocks_per_grid;
  num_threads_per_block.x = 16;
  num_threads_per_block.y = 16;
  num_blocks_per_grid.x = 4;
  num_blocks_per_grid.y = 4;
  img_reducer_ = new rmd_ImageReducer(env, 16, 16, 4, 4);

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = ref_img_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = ref_img_.width;
    res_desc.res.pitch2D.height = ref_img_.height;
    res_desc.res.pitch2D.pitchInBytes = ref_img_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&ref_img_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = curr_img_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = curr_img_.width;
    res_desc.res.pitch2D.height = curr_img_.height;
    res_desc.res.pitch2D.pitchInBytes = curr_img_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&curr_img_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = mu_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = mu_.width;
    res_desc.res.pitch2D.height = mu_.height;
    res_desc.res.pitch2D.pitchInBytes = mu_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&mu_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = sigma_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = sigma_.width;
    res_desc.res.pitch2D.height = sigma_.height;
    res_desc.res.pitch2D.pitchInBytes = sigma_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&sigma_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = a_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = a_.width;
    res_desc.res.pitch2D.height = a_.height;
    res_desc.res.pitch2D.pitchInBytes = a_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&a_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = b_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = b_.width;
    res_desc.res.pitch2D.height = b_.height;
    res_desc.res.pitch2D.pitchInBytes = b_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&b_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = convergence_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<int>();
    res_desc.res.pitch2D.width = convergence_.width;
    res_desc.res.pitch2D.height = convergence_.height;
    res_desc.res.pitch2D.pitchInBytes = convergence_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&convergence_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = epipolar_matches_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float2>();
    res_desc.res.pitch2D.width = epipolar_matches_.width;
    res_desc.res.pitch2D.height = epipolar_matches_.height;
    res_desc.res.pitch2D.pitchInBytes = epipolar_matches_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModeLinear;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&epipolar_matches_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = sum_templ_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = sum_templ_.width;
    res_desc.res.pitch2D.height = sum_templ_.height;
    res_desc.res.pitch2D.pitchInBytes = sum_templ_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&sum_templ_tex_, &res_desc, &tex_desc, NULL);
  }

  {
    cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypePitch2D;
    res_desc.res.pitch2D.devPtr = const_templ_denom_.data;
    res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
    res_desc.res.pitch2D.width = const_templ_denom_.width;
    res_desc.res.pitch2D.height = const_templ_denom_.height;
    res_desc.res.pitch2D.pitchInBytes = const_templ_denom_.pitch;

    cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.normalizedCoords = 0;

    cudaCreateTextureObject(&const_templ_denom_tex_, &res_desc, &tex_desc, NULL);
  }
}

rmd::SeedMatrix::~SeedMatrix()
{
  rmd_mvs_DeviceData_free(&dev_data_);
  delete img_reducer_;
  cudaDestroyTextureObject(ref_img_tex_);
  cudaDestroyTextureObject(curr_img_tex_);
  cudaDestroyTextureObject(mu_tex_);
  cudaDestroyTextureObject(sigma_tex_);
  cudaDestroyTextureObject(a_tex_);
  cudaDestroyTextureObject(b_tex_);
  cudaDestroyTextureObject(convergence_tex_);
  cudaDestroyTextureObject(epipolar_matches_tex_);
  cudaDestroyTextureObject(g_tex_);
  cudaDestroyTextureObject(sum_templ_tex_);
  cudaDestroyTextureObject(const_templ_denom_tex_);
}

bool rmd::SeedMatrix::setReferenceImage(
    float* host_ref_img_align_row_maj,
    const rmd_SE3 &T_curr_world,
    const float &min_depth,
    const float &max_depth)
{
  // Upload reference image to device memory
  rmd_DeviceImage_setDevData(&ref_img_, host_ref_img_align_row_maj);
  // Set scene parameters
  dev_data_.scene.min_depth    = min_depth;
  dev_data_.scene.max_depth    = max_depth;
  dev_data_.scene.avg_depth    = (min_depth+max_depth)/2.0f;
  dev_data_.scene.depth_range  = max_depth - min_depth;
  dev_data_.scene.sigma_sq_max = dev_data_.scene.depth_range * dev_data_.scene.depth_range / 36.0f;
  // Algorithm parameters
  dev_data_.eta_inlier  = 0.7f;
  dev_data_.eta_outlier = 0.05f;
  dev_data_.epsilon     = dev_data_.scene.depth_range / 1000.0f;
  // Copy data to device memory
  rmd_mvs_DeviceData_setDevData(&dev_data_);

  T_world_ref_ = rmd_SE3_inv(&T_curr_world);

  rmd_seedInitKernel<<<dim_grid_, dim_block_>>>(dev_data_.dev_ptr, ref_img_tex_);
  cudaDeviceSynchronize();

  return true;
}

bool rmd::SeedMatrix::update(
    float *host_curr_img_align_row_maj,
    const rmd_SE3 &T_curr_world)
{
  const rmd_SE3 T_curr_ref = rmd_SE3_mulWithSE3(&T_curr_world, &T_world_ref_);
  dist_from_ref_ = norm(rmd_SE3_getTranslation(&T_curr_ref));

  // Upload current image to device memory
  rmd_DeviceImage_setDevData(&curr_img_, host_curr_img_align_row_maj);

  // Assest current convergence status
  rmd_seedCheckKernel<<<dim_grid_, dim_block_>>>(dev_data_.dev_ptr, mu_tex_, sigma_tex_, a_tex_, b_tex_);
  cudaError err = cudaDeviceSynchronize();
  CHECK_ERROR(err, "SeedMatrix: unable to synchronize device");

  // Establish epipolar correspondences
  // call epipolar matching kernel
  rmd_copyImgSzToConst(&host_img_size_);

  rmd_seedEpipolarMatchKernel<<<dim_grid_, dim_block_>>>(dev_data_.dev_ptr, 
                                                         T_curr_ref,
                                                         ref_img_tex_,
                                                         curr_img_tex_,
                                                         mu_tex_,
                                                         sigma_tex_,
                                                         convergence_tex_,
                                                         sum_templ_tex_,
                                                         const_templ_denom_tex_);
  err = cudaDeviceSynchronize();
  CHECK_ERROR(err, "SeedMatrix: unable to synchronize device");

  rmd_seedUpdateKernel<<<dim_grid_, dim_block_>>>(dev_data_.dev_ptr, 
                                                  rmd_SE3_inv(&T_curr_ref),
                                                  mu_tex_,
                                                  sigma_tex_,
                                                  a_tex_,
                                                  b_tex_,
                                                  convergence_tex_,
                                                  epipolar_matches_tex_);

  return true;
}

void rmd::SeedMatrix::downloadDepthmap(float *host_depthmap_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&mu_, host_depthmap_align_row_maj);
}

void rmd::SeedMatrix::downloadConvergence(int *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&convergence_, host_align_row_maj);
}

const rmd_DeviceImage& rmd::SeedMatrix::getMu() const
{
  return mu_;
}

const rmd_DeviceImage& rmd::SeedMatrix::getSigmaSq() const
{
  return sigma_;
}

const rmd_DeviceImage& rmd::SeedMatrix::getA() const
{
  return a_;
}

const rmd_DeviceImage& rmd::SeedMatrix::getB() const
{
  return b_;
}

const rmd_DeviceImage& rmd::SeedMatrix::getConvergence() const
{
  return convergence_;
}

size_t rmd::SeedMatrix::getConvergedCount() const
{
  int* host_ptr = new int[convergence_.element_size*convergence_.width*convergence_.height];
  cudaError err = cudaMemcpy2D(
    host_ptr,
    convergence_.width*convergence_.element_size,
    convergence_.data,
    convergence_.pitch,
    convergence_.width*convergence_.element_size,
    convergence_.height,
    cudaMemcpyDeviceToHost);
  CHECK_ERROR(err, "getConvergedCount: cudaMemcpy2D() failed.");
  size_t count = img_reducer_->countEqual(host_ptr, convergence_.width, convergence_.width, convergence_.height, ConvergenceStates_CONVERGED);
  delete host_ptr;
  return count;
}

float rmd::SeedMatrix::getDistFromRef() const
{
  return dist_from_ref_;
}

#if RMD_BUILD_TESTS
void rmd::SeedMatrix::downloadSigmaSq(float *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&sigma_, host_align_row_maj);
}
void rmd::SeedMatrix::downloadA(float *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&a_, host_align_row_maj);
}
void rmd::SeedMatrix::downloadB(float *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&b_, host_align_row_maj);
}
void rmd::SeedMatrix::downloadSumTempl(float *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&sum_templ_, host_align_row_maj);
}
void rmd::SeedMatrix::downloadConstTemplDenom(float *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&const_templ_denom_, host_align_row_maj);
}
void rmd::SeedMatrix::downloadEpipolarMatches(float2 *host_align_row_maj) const
{
  rmd_DeviceImage_getDevData(&epipolar_matches_, host_align_row_maj);
}
#endif
