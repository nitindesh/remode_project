// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <rmd/depthmap_denoiser.cuh>
#include <cuda_toolkit/helper_math.h>

#include "depthmap_denoiser_kernels.cu"

void rmd_denoise_DeviceData_init(
    rmd_denoise_DeviceData* device_data,
    rmd_DeviceImage* u_dev_ptr,
    rmd_DeviceImage* p_dev_ptr,
    rmd_DeviceImage* g_dev_ptr,
    size_t w,
    size_t h)
{
  device_data->L = sqrt(8.0f);
  device_data->tau = 0.02f;
  device_data->sigma = (1 / (device_data->L*device_data->L)) / device_data->tau;
  device_data->theta = 0.5f;
  device_data->u = u_dev_ptr;
  device_data->p = p_dev_ptr;
  device_data->g = g_dev_ptr;
  device_data->width = w;
  device_data->height = h;
  device_data->lambda = 0.2f;
}

rmd::DepthmapDenoiser::DepthmapDenoiser(CLEnv& env, SeedMatrix& seeds, size_t width, size_t height) 
  : env_(env)
  , seeds_(seeds)
{
  rmd_DeviceImage_init(&u_, sizeof(float), width, height);
  rmd_DeviceImage_init(&p_, sizeof(float2), width, height);
  rmd_DeviceImage_init(&g_, sizeof(float), width, height);

  host_ptr = new rmd_denoise_DeviceData;
  rmd_denoise_DeviceData_init(
      host_ptr,
      u_.dev_ptr,
      p_.dev_ptr,
      g_.dev_ptr,
      width,
      height);
  const cudaError err = cudaMalloc(
      &dev_ptr,
      sizeof(*host_ptr));
  CHECK_ERROR(err, "DeviceData, cannot allocate device memory.");

  dim_block_.x = 16;
  dim_block_.y = 16;
  dim_grid_.x = (width  + dim_block_.x - 1) / dim_block_.x;
  dim_grid_.y = (height + dim_block_.y - 1) / dim_block_.y;

  host_img_size_.width  = width;
  host_img_size_.height = height;

  cudaResourceDesc res_desc;
  memset(&res_desc, 0, sizeof(res_desc));
  res_desc.resType = cudaResourceTypePitch2D;
  res_desc.res.pitch2D.devPtr = g_.data;
  res_desc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
  res_desc.res.pitch2D.width = g_.width;
  res_desc.res.pitch2D.height = g_.height;
  res_desc.res.pitch2D.pitchInBytes = g_.pitch;

  cudaTextureDesc tex_desc;
  memset(&tex_desc, 0, sizeof(tex_desc));
  tex_desc.addressMode[0] = cudaAddressModeClamp;
  tex_desc.addressMode[1] = cudaAddressModeClamp;
  tex_desc.filterMode = cudaFilterModeLinear;
  tex_desc.readMode = cudaReadModeElementType;
  tex_desc.normalizedCoords = 0;

  cudaCreateTextureObject(&g_tex_, &res_desc, &tex_desc, NULL);
}

rmd::DepthmapDenoiser::~DepthmapDenoiser()
{
  delete host_ptr;
  const cudaError err = cudaFree(dev_ptr);
  CHECK_ERROR(err, "DeviceData, unable to free device memory.");
  cudaDestroyTextureObject(g_tex_);
}

void rmd::DepthmapDenoiser::denoise(float *host_denoised, float lambda, int iterations)
{
  // large_sigma_sq must be set before calling this method
  if(host_ptr->large_sigma_sq < 0.0f)
  {
    std::cerr << "ERROR: setLargeSigmaSq must be called before this method" << std::endl;
    return;
  }
  host_ptr->lambda = lambda;
  cudaError err = cudaMemcpy(
        dev_ptr,
        host_ptr,
        sizeof(*host_ptr),
        cudaMemcpyHostToDevice);
  CHECK_ERROR(err, "DeviceData, cannot copy to device memory.");

  err = cudaMemcpyToSymbol(c_img_size, &host_img_size_, sizeof(rmd_Size));
  CHECK_ERROR(err, "DepthmapDenoiser: unable to copy to const memory");

  rmd_denoise_computeWeightsKernel<<<dim_grid_, dim_block_>>>(dev_ptr, seeds_.sigma_tex_, seeds_.a_tex_, seeds_.b_tex_);

  u_ = seeds_.getMu();
  rmd_DeviceImage_zero(&p_);

  for(int i = 0; i < iterations; ++i)
  {
    rmd_denoise_updateTVL1PrimalDualKernel<<<dim_grid_, dim_block_>>>(dev_ptr, seeds_.mu_tex_, g_tex_);
  }
  rmd_DeviceImage_getDevData(&u_, host_denoised);
}

void rmd::DepthmapDenoiser::setLargeSigmaSq(float depth_range)
{
  host_ptr->large_sigma_sq =  depth_range * depth_range / 72.0f;
}
