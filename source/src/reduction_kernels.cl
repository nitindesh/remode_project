// Templated kernels
kernel
void rmd_reductionSumKernel(global int* out_dev_ptr,
                            int out_stride,
                            global const int* in_dev_ptr,
                            int in_stride,
                            int n,
                            int m,
                            global int* s_partial)
{
  //int* s_partial = s_int;

  int sum = 0;

  // Sum over 2D thread grid, use (x,y) indices
  for(int x = get_group_id(0) * get_local_size(0) + get_local_id(0);
      x < n;
      x += get_local_size(0)*get_num_groups(0))
  {
    for(int y = get_group_id(1) * get_local_size(1) + get_local_id(1);
        y < m;
        y += get_local_size(1)*get_num_groups(1))
    {
      sum += in_dev_ptr[y*in_stride+x];
    }
  }
  // Sums are written to shared memory, single index
  s_partial[get_local_id(0)*get_local_size(0)+get_local_id(0)] = sum;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Reduce over block sums stored in shared memory
  // Start using half the block threads,
  // halve the active threads at each iteration
  const int tid = get_local_id(1)*get_local_size(0)+get_local_id(0);
  for (int num_active_threads = (get_local_size(0)*get_local_size(1))>>1;
       num_active_threads;
       num_active_threads >>= 1 ) {
    if ( tid < num_active_threads)
    {
      s_partial[tid] += s_partial[tid+num_active_threads];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  // Thread 0 writes the result for the block
  if(0 == tid)
  {
    out_dev_ptr[get_group_id(1)*out_stride+get_group_id(0)] = s_partial[0];
  }
}

kernel
void rmd_reductionCountEqKernel(global int* out_dev_ptr,
                                int out_stride,
                                global const int* in_dev_ptr,
                                int in_stride,
                                int n,
                                int m,
                                int value,
                                global int* s_partial)
{
  //int* s_partial = s_int;

  int count = 0;

  // Sum over 2D thread grid, use (x,y) indices
  for(int x = get_group_id(0) * get_local_size(0) + get_local_id(0);
      x < n;
      x += get_local_size(0)*get_num_groups(0))
  {
    for(int y = get_group_id(1) * get_local_size(1) + get_local_id(1);
        y < m;
        y += get_local_size(1)*get_num_groups(1))
    {
      if(value == in_dev_ptr[y*in_stride+x])
      {
        count += 1;
      }
    }
  }
  // Sums are written to shared memory, single index
  s_partial[get_local_id(1)*get_local_size(0)+get_local_id(0)] = count;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Reduce over block sums stored in shared memory
  // Start using half the block threads,
  // halve the active threads at each iteration
  const int tid = get_local_id(1)*get_local_size(0)+get_local_id(0);
  for (int num_active_threads = (get_local_size(0)*get_local_size(1))>>1;
       num_active_threads;
       num_active_threads >>= 1 ) {
    if (tid < num_active_threads)
    {
      s_partial[tid] += s_partial[tid+num_active_threads];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  // Thread 0 writes the result for the block
  if(0 == tid)
  {
    out_dev_ptr[get_group_id(1)*out_stride+get_group_id(0)] = s_partial[0];
  }
}