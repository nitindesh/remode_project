// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <rmd/denoise_device_data.cuh>

#define rmd_denoise_max(a, b) ((a) > (b) ? (a) : (b))
#define rmd_denoise_min(a, b) ((a) < (b) ? (a) : (b))

__constant__ struct rmd_Size c_img_size;

__global__
void rmd_denoise_computeWeightsKernel(rmd_denoise_DeviceData* dev_ptr,
                                      cudaTextureObject_t sigma_tex,
                                      cudaTextureObject_t a_tex,
                                      cudaTextureObject_t b_tex)
{
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  float xx = x+0.5f;
  float yy = y+0.5f;

  if (x < c_img_size.width && y < c_img_size.height)
  {
    const float E_pi = tex2D<float>(a_tex, xx, yy) / (tex2D<float>(a_tex, xx, yy) + tex2D<float>(b_tex, xx, yy));
    *rmd_DeviceImage_atXY_as_float(dev_ptr->g, x, y) = rmd_denoise_max((E_pi*tex2D<float>(sigma_tex, xx, yy)+(1.0f-E_pi)*dev_ptr->large_sigma_sq)/dev_ptr->large_sigma_sq, 1.0f);
  }
}

__global__
void rmd_denoise_updateTVL1PrimalDualKernel(rmd_denoise_DeviceData* dev_ptr, cudaTextureObject_t mu_tex, cudaTextureObject_t g_tex)
{
  const int x = blockIdx.x * blockDim.x + threadIdx.x;
  const int y = blockIdx.y * blockDim.y + threadIdx.y;

  const float xx = x+0.5f;
  const float yy = y+0.5f;

  if (x < c_img_size.width && y < c_img_size.height)
  {
    const float noisy_depth = tex2D<float>(mu_tex, xx, yy );
    const float old_u = *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y);
    const float g = tex2D<float>(g_tex, xx, yy);
    // update dual
    const float2 p = *rmd_DeviceImage_atXY_as_float2(dev_ptr->p, x, y);
    float2 grad_uhead = make_float2(0.0f, 0.0f);
    const float current_u = *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y);
    grad_uhead.x = *rmd_DeviceImage_atXY_as_float(dev_ptr->u, rmd_denoise_min(c_img_size.width-1, x+1), y)  - current_u;
    grad_uhead.y = *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, rmd_denoise_min(c_img_size.height-1, y+1)) - current_u;
    const float2 temp_p = g * grad_uhead * dev_ptr->sigma + p;
    const float sqrt_p = sqrt(temp_p.x * temp_p.x + temp_p.y * temp_p.y);
    *rmd_DeviceImage_atXY_as_float2(dev_ptr->p, x, y) = temp_p / rmd_denoise_max(1.0f, sqrt_p);

    __syncthreads();

    // update primal
    float2 current_p = *rmd_DeviceImage_atXY_as_float2(dev_ptr->p, x, y);
    float2 w_p = *rmd_DeviceImage_atXY_as_float2(dev_ptr->p, rmd_denoise_max(0, x-1), y);
    float2 n_p = *rmd_DeviceImage_atXY_as_float2(dev_ptr->p, x, rmd_denoise_max(0, y-1));
    if (x == 0)
      w_p.x = 0.0f;
    else if (x >= c_img_size.width - 1)
      current_p.x = 0.0f;
    if (y == 0)
      n_p.y = 0.0f;
    else if (y >= c_img_size.height - 1)
      current_p.y = 0.0f;
    const float divergence = current_p.x - w_p.x + current_p.y - n_p.y;

    float temp_u = old_u + dev_ptr->tau * g * divergence;
    if ((temp_u - noisy_depth) > (dev_ptr->tau * dev_ptr->lambda))
    {
      *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y) = temp_u - dev_ptr->tau * dev_ptr->lambda;
    }
    else if ((temp_u - noisy_depth) < (-dev_ptr->tau * dev_ptr->lambda))
    {
      *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y) = temp_u + dev_ptr->tau * dev_ptr->lambda;
    }
    else
    {
      *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y) = noisy_depth;
    }
    *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y) = *rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y)
        + dev_ptr->theta * (*rmd_DeviceImage_atXY_as_float(dev_ptr->u, x, y) - old_u);
  }
  __syncthreads();
}