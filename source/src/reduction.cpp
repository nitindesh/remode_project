// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <rmd/reduction.h>

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <cstring>

rmd_ImageReducer::rmd_ImageReducer(CLEnv& env,
                                   int num_threads_per_block_x,
                                   int num_threads_per_block_y,
                                   int num_blocks_per_grid_x,
                                   int num_blocks_per_grid_y)
  : env_(env)
  , block_dim_x_(num_threads_per_block_x)
  , block_dim_y_(num_threads_per_block_y)
  , grid_dim_x_(num_blocks_per_grid_x)
  , grid_dim_y_(num_blocks_per_grid_y)
{
  cl_int err;

  std::string kernel_src = ReadSource("reduction_kernels.cl");
  const char* kernel_src_str = kernel_src.c_str();

  cl_program program = clCreateProgramWithSource(env_.context, 1, (const char**)&kernel_src_str, NULL, &err);
  CL_CHECK(err);

  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  CL_CHECK(err);

  reductionSumKernel_ = clCreateKernel(program, "rmd_reductionSumKernel", &err);
  CL_CHECK(err);

  reductionCountEqKernel_ = clCreateKernel(program, "rmd_reductionCountEqKernel", &err);
  CL_CHECK(err);

  // Compute required amount of shared memory.
  sh_mem_size_ = block_dim_x_ * block_dim_y_ * sizeof(int);

  // Allocate intermediate result.
  dev_partial_ = clCreateBuffer(env_.context, CL_MEM_READ_WRITE, sizeof(int) * grid_dim_x_ * grid_dim_y_, NULL, &err);
  CL_CHECK(err);

  dev_partial_stride_ = grid_dim_x_;

  // Allocate final result.
  dev_final_ = clCreateBuffer(env_.context, CL_MEM_READ_WRITE, sizeof(int), NULL, &err);
  CL_CHECK(err);

  dev_shared_ = clCreateBuffer(env_.context, CL_MEM_READ_WRITE, sh_mem_size_, NULL, &err);
  CL_CHECK(err);
}

rmd_ImageReducer::~rmd_ImageReducer()
{
  CL_CHECK(clReleaseMemObject(dev_final_));
  CL_CHECK(clReleaseMemObject(dev_partial_));
  CL_CHECK(clReleaseMemObject(dev_shared_));
}

// Count elements equal to 'value'.
// First count over the thread grid, then perform a reduction sum on a single thread block.
size_t rmd_ImageReducer::countEqual(const int* in_img_data,
                                    size_t in_img_stride,
                                    size_t in_img_width,
                                    size_t in_img_height,
                                    int value)
{
  cl_int err;

  // Run the first kernel.
  {
    cl_command_queue commands = clCreateCommandQueue(env_.context, env_.device_id, 0, &err);
    CL_CHECK(err);

    cl_mem in_img_buffer = clCreateBuffer(env_.context, CL_MEM_READ_WRITE, sizeof(int) * in_img_width * in_img_height, NULL, &err);
    CL_CHECK(err);

    CL_CHECK(clEnqueueWriteBuffer(commands, in_img_buffer, CL_TRUE, 0, sizeof(int) * in_img_width * in_img_height, in_img_data, 0, NULL, NULL));

    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 0, sizeof(cl_mem), &dev_partial_));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 1, sizeof(int), &dev_partial_stride_));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 2, sizeof(cl_mem), &in_img_buffer));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 3, sizeof(int), &in_img_stride));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 4, sizeof(int), &in_img_width));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 5, sizeof(int), &in_img_height));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 6, sizeof(int), &value));
    CL_CHECK(clSetKernelArg(reductionCountEqKernel_, 7, sizeof(cl_mem), &dev_shared_));

    // @TODO: Optimize this.
    size_t global = grid_dim_x_ * grid_dim_y_;
    size_t local = 2;
    CL_CHECK(clEnqueueNDRangeKernel(commands, reductionCountEqKernel_, 1, NULL, &global, &local, 0, NULL, NULL));

    clFinish(commands);

    clReleaseMemObject(in_img_buffer);
    clReleaseCommandQueue(commands);
  }

  // Run the second kernel.
  {
    cl_command_queue commands = clCreateCommandQueue(env_.context, env_.device_id, 0, &err);
    CL_CHECK(err);

    int dev_final_stride = 0;

    CL_CHECK(clSetKernelArg(reductionSumKernel_, 0, sizeof(cl_mem), &dev_final_));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 1, sizeof(int), &dev_final_stride));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 2, sizeof(cl_mem), &dev_partial_));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 3, sizeof(int), &dev_partial_stride_));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 4, sizeof(int), &grid_dim_x_));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 5, sizeof(int), &grid_dim_y_));
    CL_CHECK(clSetKernelArg(reductionSumKernel_, 6, sizeof(cl_mem), &dev_shared_));

    // @TODO: Optimize this.
    size_t global = grid_dim_x_ * grid_dim_y_;
    size_t local = 2;
    CL_CHECK(clEnqueueNDRangeKernel(commands, reductionSumKernel_, 1, NULL, &global, &local, 0, NULL, NULL));

    int h_count;
    CL_CHECK(clEnqueueReadBuffer(commands, dev_final_, CL_TRUE, 0, sizeof(int), &h_count, 0, NULL, NULL));

    clFinish(commands);

    clReleaseCommandQueue(commands);

    return static_cast<size_t>(h_count);
  }
}