// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_SEED_CHECK_CU
#define RMD_SEED_CHECK_CU

#include <rmd/convergence_states.cuh>
#include <rmd/mvs_device_data.cuh>

__global__
void rmd_seedCheckKernel(rmd_mvs_DeviceData* dev_ptr,
                         cudaTextureObject_t mu_tex,
                         cudaTextureObject_t sigma_tex,
                         cudaTextureObject_t a_tex,
                         cudaTextureObject_t b_tex)
{
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  if(x >= dev_ptr->width || y >= dev_ptr->height)
    return;

  if(x > dev_ptr->width-RMD_CORR_PATCH_SIDE-1 || y > dev_ptr->height-RMD_CORR_PATCH_SIDE-1 ||
     x < RMD_CORR_PATCH_SIDE || y < RMD_CORR_PATCH_SIDE)
  {
    *rmd_DeviceImage_atXY_as_int(dev_ptr->convergence, x, y) = ConvergenceStates_BORDER;
    return;
  }

  const float xx = x+0.5f;
  const float yy = y+0.5f;

  // Retrieve current estimations of parameters
  const float mu = tex2D<float>(mu_tex, xx, yy);
  const float sigma_sq = tex2D<float>(sigma_tex, xx, yy);
  const float a = tex2D<float>(a_tex, xx, yy);
  const float b = tex2D<float>(b_tex, xx, yy);

  // if E(inlier_ratio) > eta_inlier && sigma_sq < epsilon
  if( ((a / (a + b)) > dev_ptr->eta_inlier)
      && (sigma_sq < dev_ptr->epsilon) )
  { // The seed converged
    *rmd_DeviceImage_atXY_as_int(dev_ptr->convergence, x, y) = ConvergenceStates_CONVERGED;
  }
  else if((a-1) / (a + b - 2) < dev_ptr->eta_outlier)
  { // The seed failed to converge
    *rmd_DeviceImage_atXY_as_int(dev_ptr->convergence, x, y) = ConvergenceStates_DIVERGED;
  }
  else
  {
    *rmd_DeviceImage_atXY_as_int(dev_ptr->convergence, x, y) = ConvergenceStates_UPDATE;
  }
}

#endif
