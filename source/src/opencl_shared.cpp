#include <rmd/opencl_shared.h>

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>

#include <unistd.h>
#include <sys/stat.h>

void CheckClError(cl_int err, const char* expr, const char* file, int line)
{
  if (err == CL_SUCCESS) return;
  std::cerr << "CL_CHECK(" << expr << ") != CL_SUCCESS [error code = " << err << "] at file '" << file << "', line " << line << '\n';
  abort();
}

std::string ReadSource(const std::string& file_path) 
{
  const char* ros_package_path = std::getenv("ROS_PACKAGE_PATH");
  if (ros_package_path == NULL)
    throw ReadSourceError("ROS_PACKAGE_PATH is not defined. Can't read OpenCL source files.");
  
  std::istringstream iss(ros_package_path);
  std::string path;
  bool found = false;
  while (std::getline(iss, path, ':'))
  {
    path += "/remode_project/source/src/" + file_path;
    if (access(path.c_str(), F_OK) != -1)
    {
      found = true;
      break;
    }
  }
  if (!found)
    throw ReadSourceError("Could not find '" + path + "'");

  std::ifstream file(path.c_str(), std::ios::binary | std::ios::ate);
  if (!file.is_open())
    throw ReadSourceError("Could not open '" + path + "'");
  int file_size = file.tellg();
  file.seekg(0, std::ios::beg);
  std::unique_ptr<char[]> buffer(new char[file_size]);
  memset(buffer.get(), 0, file_size);
  file.read(buffer.get(), file_size);
  return std::string(buffer.get(), file_size);
}