// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include <CL/cl.hpp>
#include <rmd/check_cuda_device.cuh>
#include <rmd/depthmap_node.h>
#include <ros/ros.h>

#include <rmd/opencl_shared.h>

#include <cstdlib>
#include <tuple>

// OpenCL initialization and context creation code.
static std::tuple<cl::Context, cl::Device> initOpenCL()
{
  // NOTE: Although this code uses the OpenCL C++ API, after we construct the 
  // CLEnv we use the C API.
  //
  // @TODO: Do this code with C API later.

  std::cout << "Checking available platforms...\n";
  std::vector<cl::Platform> all_platforms;
  cl::Platform::get(&all_platforms);
  if (all_platforms.size() == 0) {
    std::cerr << "No platforms found. Check OpenCL installation.\n";
    exit(EXIT_FAILURE);
  }
  auto default_platform = all_platforms[0];
  std::cout << "Using platform: \"" << default_platform.getInfo<CL_PLATFORM_NAME>() << "\"\n";

  std::cout << "Checking available OpenCL-capable devices...\n";
  std::vector<cl::Device> all_devices;
  default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
  if (all_devices.size() == 0) {
    std::cerr << "No available OpenCL-capable devices found.\n";
    exit(EXIT_FAILURE);
  }
  auto default_device = all_devices[0];
  
  std::cout << "Using device 0: \"" << default_device.getInfo<CL_DEVICE_NAME>() << ' ' << default_device.getInfo<CL_DEVICE_VERSION>() << "\"\n\n";
  std::cout << "Device Max Work Item Dimensions: " << default_device.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>() << '\n';
  std::cout << "Device Work Item Sizes: ";
  std::vector<size_t> work_item_sizes = default_device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  for (const auto& work_item_size : work_item_sizes) {
    std::cout << work_item_size << ' ';
  }
  std::cout << '\n';
  std::cout << "Device Vendor: " << default_device.getInfo<CL_DEVICE_VENDOR>() << '\n';
  std::cout << "Device Profile: " << default_device.getInfo<CL_DEVICE_PROFILE>() << '\n';
  std::cout << "Device Version: " << default_device.getInfo<CL_DEVICE_VERSION>() << '\n';
  std::cout << "Driver Version: " << default_device.getInfo<CL_DRIVER_VERSION>() << '\n';
  std::cout << "Device OpenCL C Version: " << default_device.getInfo<CL_DEVICE_OPENCL_C_VERSION>() << '\n';
  std::cout << "Device Extensions: " << default_device.getInfo<CL_DEVICE_EXTENSIONS>() << '\n';
  std::cout << '\n';

  return std::make_tuple(cl::Context({default_device}), default_device);
}

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  auto ret = initOpenCL();
  CLEnv env = {std::get<0>(ret)(), std::get<1>(ret)()};

  ros::init(argc, argv, "rpg_open_remode");
  ros::NodeHandle nh;
  rmd::DepthmapNode dm_node(env, nh);
  if(!dm_node.init())
  {
    ROS_ERROR("Could not initialize DepthmapNode. Shutting down node...");
    return EXIT_FAILURE;
  }

  std::string dense_input_topic("/svo/dense_input");
  ros::Subscriber dense_input_sub = nh.subscribe(
        dense_input_topic,
        1,
        &rmd::DepthmapNode::denseInputCallback,
        &dm_node);

  ros::Rate loop_rate(30);
  while(ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

  return EXIT_SUCCESS;
}
