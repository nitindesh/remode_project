
#include "rmd/checks.cuh"

#include <stdio.h>
#include <stdlib.h>

void CheckErrorCode(const char* file, int line, cudaError_t err, const char* message)
{
	if (err != cudaSuccess) {
    	fprintf(stderr, "CUDA error at file %s, line %d: %s (%d)\n%s\n", file, line, cudaGetErrorString(err), err, message);
    	abort();
    }
}