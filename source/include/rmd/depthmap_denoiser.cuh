// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_DEPTHMAP_DENOISER_CUH
#define RMD_DEPTHMAP_DENOISER_CUH

#include <rmd/opencl_shared.h>
#include <rmd/device_image.cuh>
#include <rmd/denoise_device_data.cuh>
#include <rmd/seed_matrix.cuh>

namespace rmd
{

class DepthmapDenoiser
{
public:
  DepthmapDenoiser(CLEnv& env, SeedMatrix& seeds, size_t width, size_t height);
  ~DepthmapDenoiser();
  void denoise(float *host_denoised, float lambda, int iterations);
  void setLargeSigmaSq(float depth_range);
private:
  CLEnv& env_;

  SeedMatrix& seeds_;

  cudaTextureObject_t g_tex_;

  rmd_DeviceImage u_;
  rmd_DeviceImage p_;
  rmd_DeviceImage g_;

  rmd_denoise_DeviceData* host_ptr;
  rmd_denoise_DeviceData* dev_ptr;

  dim3 dim_block_;
  dim3 dim_grid_;

  // Image size to be copied to constant memory
  rmd_Size host_img_size_;
};

} // rmd namespace

#endif // RMD_DEPTHMAP_DENOISER_CUH
