#ifndef RMD_CONVERGENCE_STATES_CUH
#define RMD_CONVERGENCE_STATES_CUH

typedef enum ConvergenceStates
{
  ConvergenceStates_UPDATE = 0,
  ConvergenceStates_CONVERGED,
  ConvergenceStates_BORDER,
  ConvergenceStates_DIVERGED,
  ConvergenceStates_NO_MATCH,
  ConvergenceStates_NOT_VISIBLE
} ConvergenceStates;

#endif /* RMD_CONVERGENCE_STATES_CUH */