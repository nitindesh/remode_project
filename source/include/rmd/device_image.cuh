// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DEVICE_IMAGE_CUH
#define DEVICE_IMAGE_CUH

#include <cuda_runtime.h>
#include <rmd/checks.cuh>

typedef struct rmd_Size
{
  int width;
  int height;
} rmd_Size;

typedef struct rmd_DeviceImage
{
  // fields
  size_t width;
  size_t height;
  size_t pitch;
  size_t stride;
  void* data;
  size_t element_size;
  rmd_DeviceImage* dev_ptr;
} rmd_DeviceImage;

__host__ __forceinline__
void rmd_DeviceImage_init(rmd_DeviceImage* self, size_t element_size, size_t width, size_t height)
{
  self->width = width;
  self->height = height;
  self->element_size = element_size;

  cudaError err = cudaMallocPitch(
        &self->data,
        &self->pitch,
        self->width*self->element_size,
        self->height);
  CHECK_ERROR(err, "Image: unable to allocate pitched memory.");

  self->stride = self->pitch / self->element_size;

  err = cudaMalloc(
        &self->dev_ptr,
        sizeof(*self));
  CHECK_ERROR(err, "DeviceData, cannot allocate device memory to store image parameters.");

  err = cudaMemcpy(
        self->dev_ptr,
        self,
        sizeof(*self),
        cudaMemcpyHostToDevice);
  CHECK_ERROR(err, "DeviceData, cannot copy image parameters to device memory.");
}

__host__ __forceinline__
void rmd_DeviceImage_free(rmd_DeviceImage* self)
{
  cudaError err = cudaFree(self->data);
  CHECK_ERROR(err, "Image: unable to free allocated memory.");
  err = cudaFree(self->dev_ptr);
  CHECK_ERROR(err, "Image: unable to free allocated memory.");
}

__device__ __forceinline__
int* rmd_DeviceImage_atXY_as_int(rmd_DeviceImage* self, size_t x, size_t y)
{ return &((int*)self->data)[y*self->stride+x]; }

__device__ __forceinline__
float* rmd_DeviceImage_atXY_as_float(rmd_DeviceImage* self, size_t x, size_t y)
{ return &((float*)self->data)[y*self->stride+x]; }

__device__ __forceinline__
float2* rmd_DeviceImage_atXY_as_float2(rmd_DeviceImage* self, size_t x, size_t y)
{ return &((float2*)self->data)[y*self->stride+x]; }

__host__ __forceinline__
cudaChannelFormatDesc rmd_DeviceImage_getCudaChannelFormatDesc_for_int()
{ return cudaCreateChannelDesc(sizeof(int)*8, 0, 0, 0, cudaChannelFormatKindSigned); }

__host__ __forceinline__
cudaChannelFormatDesc rmd_DeviceImage_getCudaChannelFormatDesc_for_float()
{ return cudaCreateChannelDesc(sizeof(float)*8, 0, 0, 0, cudaChannelFormatKindFloat); }

__host__ __forceinline__
cudaChannelFormatDesc rmd_DeviceImage_getCudaChannelFormatDesc_for_float2()
{ return cudaCreateChannelDesc(sizeof(float)*8, sizeof(float)*8, 0, 0, cudaChannelFormatKindFloat); }

/// Download the data from the device memory to aligned_data_row_major, a preallocated array in host memory
__host__ __forceinline__
void rmd_DeviceImage_getDevData(const rmd_DeviceImage* self, void* aligned_data_row_major)
{
  const cudaError err = cudaMemcpy2D(
        aligned_data_row_major,      // destination memory address
        self->width*self->element_size,   // pitch of destination memory
        self->data,                        // source memory address
        self->pitch,                       // pitch of source memory
        self->width*self->element_size,   // width of matrix transfer (columns in bytes)
        self->height,                      // height of matrix transfer
        cudaMemcpyDeviceToHost);
  CHECK_ERROR(err, "Image: unable to copy data from device to host.");
}

/// Upload aligned_data_row_major to device memory
__host__ __forceinline__
void rmd_DeviceImage_setDevData(rmd_DeviceImage* self, const void* aligned_data_row_major)
{
  const cudaError err = cudaMemcpy2D(
        self->data,
        self->pitch,
        aligned_data_row_major,
        self->width*self->element_size,
        self->width*self->element_size,
        self->height,
        cudaMemcpyHostToDevice);
  CHECK_ERROR(err, "Image: unable to copy data from host to device.");
}

__host__ __forceinline__
void rmd_DeviceImage_zero(rmd_DeviceImage* self)
{
  const cudaError err = cudaMemset2D(
        self->data,
        self->pitch,
        0,
        self->width*self->element_size,
        self->height);
  CHECK_ERROR(err, "Image: unable to zero.");
}

#endif // DEVICE_IMAGE_CUH
