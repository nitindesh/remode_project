// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_REDUCTION_CUH
#define RMD_REDUCTION_CUH

#include <rmd/opencl_shared.h>
#include <cstdlib>

class rmd_ImageReducer
{
public:
  rmd_ImageReducer(CLEnv& env,
                   int num_threads_per_block_x,
                   int num_threads_per_block_y,
                   int num_blocks_per_grid_x,
                   int num_blocks_per_grid_y);
  ~rmd_ImageReducer();

  // Count elements equal to 'value'
  size_t countEqual(const int* in_img_data,
                    size_t in_img_stride,
                    size_t in_img_width,
                    size_t in_img_height,
                    int value);

private:
  CLEnv& env_;
  cl_kernel reductionSumKernel_;
  cl_kernel reductionCountEqKernel_;

  int block_dim_x_;
  int block_dim_y_;
  int grid_dim_x_;
  int grid_dim_y_;
  unsigned int sh_mem_size_;
  cl_mem dev_final_;
  cl_mem dev_partial_;
  cl_mem dev_shared_;
  size_t dev_partial_pitch_;
  size_t dev_partial_stride_;
};

#endif // RMD_REDUCTION_CUH
