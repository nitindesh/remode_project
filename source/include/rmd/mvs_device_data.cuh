// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_MVS_DEVICE_DATA_CUH
#define RMD_MVS_DEVICE_DATA_CUH

#include <rmd/pinhole_camera.cuh>
#include <rmd/device_image.cuh>

typedef struct rmd_mvs_SceneData
{
  float min_depth;
  float max_depth;
  float avg_depth;
  float depth_range;
  float sigma_sq_max;
} rmd_mvs_SceneData;

#ifndef RMD_CORR_PATCH_SIDE
#define RMD_CORR_PATCH_SIDE    5
#endif
#define RMD_CORR_PATCH_OFFSET -RMD_CORR_PATCH_SIDE/2
#define RMD_CORR_PATCH_AREA    RMD_CORR_PATCH_SIDE*RMD_CORR_PATCH_SIDE

// DeviceData struct stores pointers to dev memory.
// It is allocated and set from host.
typedef struct rmd_mvs_DeviceData
{
  rmd_DeviceImage* ref_img;
  rmd_DeviceImage* curr_img;
  rmd_DeviceImage* sum_templ;
  rmd_DeviceImage* const_templ_denom;
  rmd_DeviceImage* mu;
  rmd_DeviceImage* sigma;
  rmd_DeviceImage* a;
  rmd_DeviceImage* b;
  rmd_DeviceImage* convergence;
  rmd_DeviceImage* epipolar_matches;

  rmd_PinholeCamera cam;
  float one_pix_angle;
  size_t width;
  size_t height;

  rmd_mvs_SceneData scene;

  // Algorithm parameters
  float eta_inlier;
  float eta_outlier;
  float epsilon;

  rmd_mvs_DeviceData* dev_ptr;
  bool is_dev_allocated;
} rmd_mvs_DeviceData;

__host__ __forceinline__
void rmd_mvs_DeviceData_init(rmd_mvs_DeviceData* self)
{
  self->is_dev_allocated = false;
}
__host__ __forceinline__
void rmd_mvs_DeviceData_free(rmd_mvs_DeviceData* self)
{
  if(self->is_dev_allocated)
    cudaFree(self->dev_ptr);
}
__host__ __forceinline__
void rmd_mvs_DeviceData_setDevData(rmd_mvs_DeviceData* self)
{
  if(!self->is_dev_allocated)
  {
    // Allocate device memory
    const cudaError err = cudaMalloc(&self->dev_ptr, sizeof(*self));
    CHECK_ERROR(err, "DeviceData, cannot allocate device memory to store image parameters.");
    self->is_dev_allocated = true;
  }
  // Copy data to device memory
  const cudaError err = cudaMemcpy(self->dev_ptr, self, sizeof(*self), cudaMemcpyHostToDevice);
  CHECK_ERROR(err, "DeviceData, cannot copy image parameters to device memory.");
}

#endif // RMD_MVS_DEVICE_DATA_CUH
