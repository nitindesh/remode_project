// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_DENOISE_DEVICE_DATA_CUH
#define RMD_DENOISE_DEVICE_DATA_CUH

#include <rmd/device_image.cuh>

typedef struct rmd_denoise_DeviceData
{
  float L;
  float tau;
  float sigma;
  float theta;

  rmd_DeviceImage* u;
  rmd_DeviceImage* p;
  rmd_DeviceImage* g;

  size_t width;
  size_t height;

  float large_sigma_sq;
  float lambda;
} rmd_denoise_DeviceData;

void rmd_denoise_DeviceData_init(
  rmd_denoise_DeviceData* device_data, 
  rmd_DeviceImage* u_dev_ptr,
  rmd_DeviceImage* p_dev_ptr,
  rmd_DeviceImage* g_dev_ptr,
  size_t w,
  size_t h
);

#endif // RMD_DENOISE_DEVICE_DATA_CUH
