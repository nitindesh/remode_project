// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SEED_MATRIX_CUH
#define SEED_MATRIX_CUH

#include <cuda_runtime.h>
#include <rmd/opencl_shared.h>
#include <rmd/device_image.cuh>
#include <rmd/pinhole_camera.cuh>
#include <rmd/mvs_device_data.cuh>
#include <rmd/se3.cuh>
#include <rmd/reduction.h>

namespace rmd
{

class SeedMatrix
{
public:
  SeedMatrix(
      CLEnv& env,
      const size_t &width,
      const size_t &height,
      const rmd_PinholeCamera &cam);
  ~SeedMatrix();
  bool setReferenceImage(
      float *host_ref_img_align_row_maj,
      const rmd_SE3 &T_curr_world,
      const float &min_depth,
      const float &max_depth);
  bool update(
      float *host_curr_img_align_row_maj,
      const rmd_SE3 &T_curr_world);

  void downloadDepthmap(float *host_depthmap_align_row_maj) const;
  void downloadConvergence(int *host_align_row_maj) const;

  const rmd_DeviceImage& getMu() const;
  const rmd_DeviceImage& getSigmaSq() const;
  const rmd_DeviceImage& getA() const;
  const rmd_DeviceImage& getB() const;

  const rmd_DeviceImage& getConvergence() const;

  size_t getConvergedCount() const;

  float getDistFromRef() const;

#if RMD_BUILD_TESTS
  void downloadSigmaSq(float *host_align_row_maj) const;
  void downloadA(float *host_align_row_maj) const;
  void downloadB(float *host_align_row_maj) const;
  void downloadSumTempl(float *host_align_row_maj) const;
  void downloadConstTemplDenom(float *host_align_row_maj) const;
  void downloadEpipolarMatches(float2 *host_align_row_maj) const;
#endif


  cudaTextureObject_t mu_tex_;
  cudaTextureObject_t sigma_tex_;
  cudaTextureObject_t a_tex_;
  cudaTextureObject_t b_tex_;

private:
  cudaTextureObject_t ref_img_tex_;
  cudaTextureObject_t curr_img_tex_;


  cudaTextureObject_t convergence_tex_;
  cudaTextureObject_t epipolar_matches_tex_;

  cudaTextureObject_t g_tex_;

  // Pre-computed template statistics
  cudaTextureObject_t sum_templ_tex_;
  cudaTextureObject_t const_templ_denom_tex_;

  size_t width_;
  size_t height_;
  rmd_DeviceImage ref_img_, curr_img_;
  // Template statistics for NCC (pre)computation
  rmd_DeviceImage sum_templ_, const_templ_denom_;
  // Measurement parameters
  rmd_DeviceImage mu_, sigma_, a_, b_;
  // Convergence state
  rmd_DeviceImage convergence_;
  // Epipolar matches
  rmd_DeviceImage epipolar_matches_;
  rmd_mvs_DeviceData dev_data_;
  rmd_SE3 T_world_ref_;
  float dist_from_ref_;
  // kernel config
  dim3 dim_block_;
  dim3 dim_grid_;
  // Image reduction to compute seed statistics
  rmd_ImageReducer* img_reducer_;

  // Image size to be copied to constant memory
  rmd_Size host_img_size_;
};

} // rmd namespace

#endif // SEED_MATRIX_CUH
