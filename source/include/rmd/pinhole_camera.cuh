// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_PINHOLE_CAMERA_CUH_
#define RMD_PINHOLE_CAMERA_CUH_

#include <vector_types.h>
#include <cuda_toolkit/helper_math.h>

typedef struct rmd_PinholeCamera 
{
  float fx, fy;
  float cx, cy;
} rmd_PinholeCamera;

__host__ __device__ __forceinline__
float3 rmd_PinholeCamera_cam2world(const rmd_PinholeCamera* self, const float2 uv)
{
  return make_float3((uv.x - self->cx)/self->fx, (uv.y - self->cy)/self->fy, 1.0f);
}

__host__ __device__ __forceinline__
float2 rmd_PinholeCamera_world2cam(const rmd_PinholeCamera* self, const float3 xyz)
{
  return make_float2(self->fx*xyz.x / xyz.z + self->cx, self->fy*xyz.y / xyz.z + self->cy);
}

__host__ __device__ __forceinline__
float rmd_PinholeCamera_getOnePixAngle(const rmd_PinholeCamera* self)
{
  return atan2f(1.0f, 2.0f*self->fx)*2.0f;
}

#endif // RMD_PINHOLE_CAMERA_CUH_
