#ifndef OPENCL_SHARED_H
#define OPENCL_SHARED_H

#include <CL/cl.h>

#include <stdexcept>
#include <string>

// Checks OpenCL errors.
#define CL_CHECK(arg) CheckClError(arg, #arg, __FILE__, __LINE__)

// OpenCL related variables to pass around. This structure can be further
// extended to add additional dependencies.
struct CLEnv
{
  cl_context context;
  cl_device_id device_id;
};

void CheckClError(cl_int err, const char* expr, const char* file, int line);

// Used to report errors on ReadSource.
class ReadSourceError : public std::runtime_error 
{
public:
  ReadSourceError(const std::string& message)
    : std::runtime_error(message) {}
};

// Reads an OpenCL source file.
std::string ReadSource(const std::string& file_path);

#endif // OPENCL_SHARED_H