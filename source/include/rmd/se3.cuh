// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_SE3_CUH_
#define RMD_SE3_CUH_

#include <stdio.h>
#include <cuda_toolkit/helper_math.h>

const int RMD_SE3_NUM_ROWS = 3;
const int RMD_SE3_NUM_COLS = 4;

typedef struct rmd_SE3 {
  float data[RMD_SE3_NUM_ROWS*RMD_SE3_NUM_COLS];
} rmd_SE3;

__host__ __device__ __forceinline__
float rmd_SE3_getXY(const rmd_SE3* self, int row, int col)
{
  return self->data[row*RMD_SE3_NUM_COLS+col];
}

__host__ __device__ __forceinline__
void rmd_SE3_setXY(rmd_SE3* self, int row, int col, float val)
{
  self->data[row*RMD_SE3_NUM_COLS+col] = val;
}

/// Constructor from a normalized quaternion and a translation vector
__host__ __device__ __forceinline__
void rmd_SE3_init(rmd_SE3* self, float qw, float qx, float qy, float qz, float tx, float ty, float tz)
{
  const float x  = 2*qx;
  const float y  = 2*qy;
  const float z  = 2*qz;
  const float wx = x*qw;
  const float wy = y*qw;
  const float wz = z*qw;
  const float xx = x*qx;
  const float xy = y*qx;
  const float xz = z*qx;
  const float yy = y*qy;
  const float yz = z*qy;
  const float zz = z*qz;

  rmd_SE3_setXY(self, 0, 0, 1-(yy+zz));
  rmd_SE3_setXY(self, 0, 1, xy-wz);
  rmd_SE3_setXY(self, 0, 2, xz+wy);
  rmd_SE3_setXY(self, 1, 0, xy+wz);
  rmd_SE3_setXY(self, 1, 1, 1-(xx+zz));
  rmd_SE3_setXY(self, 1, 2, yz-wx);
  rmd_SE3_setXY(self, 2, 0, xz-wy);
  rmd_SE3_setXY(self, 2, 1, yz+wx);
  rmd_SE3_setXY(self, 2, 2, 1-(xx+yy));

  rmd_SE3_setXY(self, 0, 3, tx);
  rmd_SE3_setXY(self, 1, 3, ty);
  rmd_SE3_setXY(self, 2, 3, tz);
}

/// Construct from C arrays
/// r is rotation matrix row major
/// t is the translation vector (x y z)
__host__ __device__ __forceinline__
void rmd_SE3_initFromArray(rmd_SE3* self, float* r, float* t)
{
  self->data[0]=r[0]; self->data[1]=r[1]; self->data[2] =r[2]; self->data[3] =t[0];
  self->data[4]=r[3]; self->data[5]=r[4]; self->data[6] =r[5]; self->data[7] =t[1];
  self->data[8]=r[6]; self->data[9]=r[7]; self->data[10]=r[8]; self->data[11]=t[2];
}

__host__ __device__ __forceinline__
rmd_SE3 rmd_SE3_inv(const rmd_SE3* self)
{
  rmd_SE3 result;
  result.data[0]  = self->data[0];
  result.data[1]  = self->data[4];
  result.data[2]  = self->data[8];
  result.data[4]  = self->data[1];
  result.data[5]  = self->data[5];
  result.data[6]  = self->data[9];
  result.data[8]  = self->data[2];
  result.data[9]  = self->data[6];
  result.data[10] = self->data[10];
  result.data[3]  = -self->data[0]*self->data[3] -self->data[4]*self->data[7] -self->data[8] *self->data[11];
  result.data[7]  = -self->data[1]*self->data[3] -self->data[5]*self->data[7] -self->data[9] *self->data[11];
  result.data[11] = -self->data[2]*self->data[3] -self->data[6]*self->data[7] -self->data[10]*self->data[11];
  return result;
}

__host__ __device__ __forceinline__
float3 rmd_SE3_rotate(const rmd_SE3* self, const float3 p)
{
  return make_float3(rmd_SE3_getXY(self, 0,0)*p.x + rmd_SE3_getXY(self, 0,1)*p.y + rmd_SE3_getXY(self, 0,2)*p.z,
                     rmd_SE3_getXY(self, 1,0)*p.x + rmd_SE3_getXY(self, 1,1)*p.y + rmd_SE3_getXY(self, 1,2)*p.z,
                     rmd_SE3_getXY(self, 2,0)*p.x + rmd_SE3_getXY(self, 2,1)*p.y + rmd_SE3_getXY(self, 2,2)*p.z);
}

__host__ __device__ __forceinline__
float3 rmd_SE3_translate(const rmd_SE3* self, const float3 p)
{
  return make_float3(p.x + rmd_SE3_getXY(self, 0, 3), 
                     p.y + rmd_SE3_getXY(self, 1, 3), 
                     p.z + rmd_SE3_getXY(self, 2, 3));
}

__host__ __device__ __forceinline__
float3 rmd_SE3_getTranslation(const rmd_SE3* self)
{
  return make_float3(rmd_SE3_getXY(self, 0, 3), 
                     rmd_SE3_getXY(self, 1, 3), 
                     rmd_SE3_getXY(self, 2, 3));
}

__host__ __device__ __forceinline__
rmd_SE3 rmd_SE3_mulWithSE3(const rmd_SE3* lhs, const rmd_SE3* rhs)
{
  rmd_SE3 result;
  result.data[0]  = lhs->data[0]*rhs->data[0] + lhs->data[1]*rhs->data[4] + lhs->data[2]*rhs->data[8];
  result.data[1]  = lhs->data[0]*rhs->data[1] + lhs->data[1]*rhs->data[5] + lhs->data[2]*rhs->data[9];
  result.data[2]  = lhs->data[0]*rhs->data[2] + lhs->data[1]*rhs->data[6] + lhs->data[2]*rhs->data[10];
  result.data[3]  = lhs->data[3] + lhs->data[0]*rhs->data[3] + lhs->data[1]*rhs->data[7] + lhs->data[2]*rhs->data[11];
  result.data[4]  = lhs->data[4]*rhs->data[0] + lhs->data[5]*rhs->data[4] + lhs->data[6]*rhs->data[8];
  result.data[5]  = lhs->data[4]*rhs->data[1] + lhs->data[5]*rhs->data[5] + lhs->data[6]*rhs->data[9];
  result.data[6]  = lhs->data[4]*rhs->data[2] + lhs->data[5]*rhs->data[6] + lhs->data[6]*rhs->data[10];
  result.data[7]  = lhs->data[7] + lhs->data[4]*rhs->data[3] + lhs->data[5]*rhs->data[7] + lhs->data[6]*rhs->data[11];
  result.data[8]  = lhs->data[8]*rhs->data[0] + lhs->data[9]*rhs->data[4] + lhs->data[10]*rhs->data[8];
  result.data[9]  = lhs->data[8]*rhs->data[1] + lhs->data[9]*rhs->data[5] + lhs->data[10]*rhs->data[9];
  result.data[10] = lhs->data[8]*rhs->data[2] + lhs->data[9]*rhs->data[6] + lhs->data[10]*rhs->data[10];
  result.data[11] = lhs->data[11] + lhs->data[8]*rhs->data[3] + lhs->data[9]*rhs->data[7] + lhs->data[10]*rhs->data[11];
  return result;
}

__host__ __device__ __forceinline__
float3 rmd_SE3_mulWithVec(const rmd_SE3* self, const float3 p)
{
  return rmd_SE3_translate(self, rmd_SE3_rotate(self, p));
}

__host__ __forceinline__
void rmd_SE3_printMatrix(const rmd_SE3& se3)
{
  for(size_t row=0; row < RMD_SE3_NUM_ROWS; ++row)
  {
    for(size_t col=0; col < RMD_SE3_NUM_COLS; ++col)
    {
      printf("%.9f ", rmd_SE3_getXY(&se3, row, col));
    }
    printf("\n");
  }
}

#endif // RMD_SE3_CUH_
