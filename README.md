# REMODE: Probabilistic, Monocular Dense Reconstruction in Real Time

* [Tasks to be acheived and challenges are listed in this short presentation](http://www6.in.tum.de/pub/Main/TeachingSs2016HSCDLegoCar/topicsLEGOcarCourse.pdf)
* [Open-source release: REMODE](http://www.ros.org/news/2016/02/open-source-release-remode-probabilistic-monocular-dense-reconstruction-in-real-time.html)
* [GitHub project with CUDA implementation: REMODE](https://github.com/uzh-rpg/rpg_open_remode)

## Porting CUDA to OpenCL
* http://developer.amd.com/tools-and-sdks/opencl-zone/opencl-resources/programming-in-opencl/porting-cuda-applications-to-opencl/

## Learning Resources for CUDA. This list is not exhaustive, feel free to update this list.
* [Basic Introduction to CUDA - NVidia GPU Technology Conference](http://www.nvidia.com/content/GTC-2010/pdfs/2131_GTC2010.pdf)
* [An Easy Introduction to CUDA C and C++](https://devblogs.nvidia.com/parallelforall/easy-introduction-cuda-c-and-c/)
* [Yet another basic introduction to CUDA](https://www.nvidia.com/docs/IO/116711/sc11-cuda-c-basics.pdf)
* [An Introduction to CUDA Tesla Architecture](http://mc.stanford.edu/cgi-bin/images/f/f7/Darve_cme343_cuda_1.pdf)
* [A detailed introduction to CUDA](https://people.maths.ox.ac.uk/gilesm/cuda/)
* GTC 2015 Great Talks for full Introduction to CUDA
  * [Session 1](https://www.youtube.com/watch?v=wNjYeZJGtHc)
  * [Session 2](https://www.youtube.com/watch?v=W0WSx4toOLc)
  * [Session 3](https://www.youtube.com/watch?v=c_LHHHVnW78)
  * [Session 4](https://www.youtube.com/watch?v=_f31hvfBv4s)
* Less detailed introduction to CUDA
  * [CUDA Tutorials](https://www.youtube.com/playlist?list=PLKK11Ligqititws0ZOoGk3SW-TZCar4dK)
  * [Learn CUDA in an Afternoon: an Online Hands-on Tutorial](https://www.youtube.com/watch?v=_41LCMFpsFs)

## Learning Resources for OpenCL (latest version being 1.2). This list is not exhaustive, feel free to update this list.
* [Introduction to OpenCL from NVIDIA](http://www.cc.gatech.edu/~vetter/keeneland/tutorial-2011-04-14/06-intro_to_opencl.pdf)
* [Great short course from AMD](http://developer.amd.com/tools-and-sdks/opencl-zone/opencl-resources/opencl-course-introduction-to-opencl-programming/)
* [Introduction to OpenCL Programming - Training Guide](http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2013/01/Introduction_to_OpenCL_Programming-Training_Guide-201005.pdf)
* [OpenCL Course from Oxford](https://people.maths.ox.ac.uk/gilesm/cuda/)
* [OpenCL on demand Webinars](https://www.youtube.com/playlist?list=PLVk9nlso0x0LoT7P-GVWEASBSS750gLn6)
* [OpenCL Tutorials](https://www.youtube.com/playlist?list=PLTfYiv7-a3l7mYEdjk35wfY-KQj5yVXO2)
* [A more concrete introduction - Heterogeneous Computing with OpenCL, 2nd Edition](http://store.elsevier.com/Heterogeneous-Computing-with-OpenCL/Benedict-Gaster/isbn-9780124058941/)

## Learning Resources for Computer Vision/Digital Image Processing. This list is not exhaustive, feel free to update this list.
* [Fundamentals of Computer Vision - Mubarak Shah](http://www.cse.unr.edu/~bebis/CS485/Handouts/ShahBook.pdf)
* [Great 10 min videos about Image Processing](https://www.youtube.com/playlist?list=PLZ9qNFMHZ-A79y1StvUUqgyL-O0fZh2rs)
* [Multi-View Stereo: A Tutorial - Yasutaka Furukawa](http://carlos-hernandez.org/papers/fnt_mvs_2015.pdf)

## OpenCV
* [Learning OpenCV](http://www.bogotobogo.com/cplusplus/files/OReilly%20Learning%20OpenCV.pdf)
* [Learning OpenCV 3](http://gen.lib.rus.ec/book/index.php?md5=371E3307450D14A43CF6AA673B4FE6F3)
